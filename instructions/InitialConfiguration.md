# Configuration of the working environment

It is based on an Ubuntu operating system with minimal installation by default. It is the virtual machine that I have left you in the server.

From that point on, all the necessary elements will be installed and configured to have a Python3 and R ecosystem to launch our tests.

Specifically, the basic environment that any researcher should have when working on bioinformatics problems with R and Python is going to be set up and they want their work to be reproducible and of quality.

+ IED to be able to share results, launch simple tests in a graphical way and see results. These environments usually allow the elaboration of reports with embedded code in a simple way. When we have to share the results with a supervisor/colleague it is not enough to say "I got it, right??", "my results are cool", "I'm going to publish this result!!". The results must be reproducible and replicable. Most importantly, they must be justifiable.

## Prerequisites

### Virtual machine location

Remember that the access to the network shared folder is as follows:

+ link: https://nas.tic.udc.es/share.cgi?ssid=0bbxH2a
+ user/password: fbiostudent

To avoid wasting a lot of time in class, I have already left you the machine configured and with most packages downloaded and installed. In case any of you want to be encouraged, I'll leave you the steps I've followed in the document.

### Before we start

```sh
sudo apt-get update
sudo apt-get upgrade
pip3 --version
```

### Installation of Python3

Python3 is already installed on the machine, but the pip is missing. As you have mostly used Python and even some scikit-learn, I won't stop to explain the most basic part.

the password to access the machine is **fbiostudent**

```sh
sudo apt install python3-pip python3-tk
python3 -m pip install --upgrade pip
```

### Creating an environment to work in Python

```sh
export FBIO_PATH="$HOME/fbiopy"
mkdir -p $FBIO_PATH
pip3 install --user --upgrade virtualenv
```

#### Known errors

if when trying to install the virtualenv it crashes because it can't import main de pip

```sh
sudo -H python3 -m pip uninstall pip && sudo -H apt install python3-pip --reinstall
```
and now you can install

```sh
python3 -m pip install --user --upgrade virtualenv
```
you can export ~/.local/bin to PATH

```sh
PATH=$PATH:~/.local/bin
```


#### Creating the environment

now we have to create the environment

```sh
cd $FBIO_PATH
virtualenv fbio
```

now, whenever it is necessary to activate the virtual environment, it should be:

```sh
cd $FBIO_PATH
source fbio/bin/activate
```

#### How to install a new library

```sh
pip3 install pandas
```

### Installation of git
```sh
sudo apt-get install git
```

### Installation of R
```sh
sudo apt-get update
```

we install R in Ubuntu

```sh
sudo apt install r-base
```


## Graphic environment

### Installing Jupyter
```sh
sudo python3 -m pip install --upgrade pip
source fbiopy/fbio/bin/activate
python3 -m pip install jupyter
```

### starting Jupyter
```sh
jupyter notebook notebook.ipynb
```
the last thing is the name of the book that we want to open (if known). Remove it in case there is no notebook for default behavior.

### Installing Rstudio

```sh
sudo apt install wget
wget https://download1.rstudio.org/desktop/bionic/amd64/rstudio-1.2.5001-amd64.deb
sudo dpkg -i rstudio-1.2.1578-amd64.deb
```

If it gives you dependency failures, execute:

```sh
sudo apt -f install
```

#### Installing LaTex

installing the Latex distribution for Ubuntu: 

```sh
sudo apt-get install texlive-latex-extra
```
If space is scarce, other versions can be installed:

* texlive-base - 136 MB
* texlive-latex-recommended - 177 MB
* texlive - 240 MB
* texlive-latex-extra - 404 MB
* texlive-full - 4714 MB

#### Installing Remarkable

installing remarkable (markdown editor)

```sh
wget http://remarkableapp.github.io/files/remarkable_1.87_all.deb
dpkg -i remarkable_1.87_all.deb
```

If it gives you dependency failures, execute:

```sh
sudo apt -f install
```